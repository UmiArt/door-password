import React, {useState} from 'react';
import './Numbers.css';
import {useDispatch} from "react-redux";

const Numbers = () => {

    const [data, setData] = useState("");

    const dispatch = useDispatch();

    const addNumber = () => dispatch({type: ""})

    const btnS = [];



    [9,8,7,6,5,4,3,2,1,0].forEach(item => {
        btnS.push(
            <button onClick={e => {
                setData(data + e.target.value.replace(/./gm, "*"))
            }}
            value={item}
            key={item}>{item}
            </button>
        )
    });


    return (
        <div className="wrapper">
            <input className="show-input" >
            </input>
            <div className="digits flex">
                {btnS}
                <button onClick={() => setData(data.substr(0,data.length - 1))}>Delete</button>
                <button>E</button>
            </div>
        </div>
    );
};

export default Numbers;